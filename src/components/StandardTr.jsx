import React from 'react'



export function StandardRows({respositories}) {

    return(
        respositories.map(r=> <StandardTr repository={r} />)
    )
}


export default function StandardTr({ repository }) {

    const words = [
        { key: 'org.repo_updated', value: "last updated " }
    ]

    const i18n = (key) => {
        var foundWord = words.find(w => w.key === key)
        if (foundWord) return foundWord.value
        return key
    }


    return (<tr>
        <td data-label="Standard"><a href={repository.HTMLURL}>{repository.name}</a></td>
        <td data-label="OverallLastUpdate">
            <p class="time">{i18n("org.repo_updated")} {repository.updatedUnix}</p>
        </td>
    </tr>)
}