import React from 'react';

export default function Person({ name, age, sex, skills }) {


  const getDateOfBirth = () => {

    const date = new Date()

    const dateInt = date.getFullYear()
    const dob = dateInt - age


    return dob
  }


  return (
    <div className="App" style={{ width: 400, backgroundColor: 'lemonchiffon', border: '2px solid black', padding: 24 }}>

      <h1>Person Component</h1>
      <h2>Name:  {name}</h2>
      <h3>Age: {age} </h3>
      <h4>Sex: {sex}</h4>


      date of birth is {getDateOfBirth()}

      <div style={{ backgroundColor: 'lavender', padding: 12 }}>
        <h3> Skills</h3>
        {
          skills && skills.length > 0 && skills.map(
            s => <li>{s}</li>
          )
        }

      </div>


    </div>


  );
}


