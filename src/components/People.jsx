import React from 'react';
import Person from './Person.jsx';

export default function People() {

  const people = [
    { name: "max", age: 5, sex: 'male', skills: ['asd'] },
    { name: "dave", age: 3, sex: 'male', skills: ['asdf'] },
    { name: "timmy", age: 8, sex: 'female', skills: ['asdf'] }
  ]

  const [peopleState, setPeopleState] = React.useState(people)
  // some call to a service (restfull API)



  return (

    <>
      {
        peopleState.map(p => <Person name={p.name} age={p.age} sex={p.sex} skils={p.skills} />)
      }
    </>

  );
}


