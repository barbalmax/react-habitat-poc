import React from "react";
import ReactDOM from "react-dom";
import Blue from "./components/Blue.jsx";
import Green from "./components/Green.jsx";
import QuizQuestions from "./components/QuizQuestions.jsx";
import Person from "./components/Person.jsx";
import People from "./components/People.jsx";
import ReactHabitat from "react-habitat";
import StandardTr, { StandardRows } from "./components/StandardTr.jsx";

class MyApp extends ReactHabitat.Bootstrapper {
  constructor() {
    super();

    // TODO, ideally, eventually react will manage the i18n
    //  -> however theres possible there will be a hand over period so will
    // make sense to translate the existing one from the back end code for now

    window.i18nBarbal = [];

    window.updateHabitat = this.update.bind(this);

    // Create a new container builder:
    const builder = new ReactHabitat.ContainerBuilder();

    // Register a component:
    builder.register(Blue).as("Blue");
    builder.register(Green).as("Green");

    builder.register(QuizQuestions).as("QuizQuestions");
    builder.register(Person).as("Person");
    builder.register(People).as("People");

    builder.register(StandardRows).as("StandardRows");

    builder
      .register(StandardTr)
      .as("StandardTr")
      .withDefaultProps({
        repository: {
          HTMLURL: "https://www.google.com",
          name: "default repo name",
          updatedUnix: "023042034",
        },
      });

    // Or register a component to load on demand asynchronously:
    //builder.registerAsync(() => System.import('./AnotherReactComponent')).as('AnotherReactComponent');

    // Finally, set the container:
    this.setContainer(builder.build());
  }
}

// Always export a 'new' instance so it immediately evokes:
export default new MyApp();
